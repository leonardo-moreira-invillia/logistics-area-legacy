## Logistics Area Legacy

This project intents to manage the legacy of areas from fleet to the logistics-area project.

To achieve this goal, this Spring Boot application reads  from fleet database, specifically subset tables,
and insert that in the logistic-area format.

### Compiling

To compile the project, run `mvn clean package`.

### Database Connection

This project must connect on the two databases:
    Fleet database
    Logisctic area database

Therefore, connection properties must be set on `application.properties` file as following:

```
spring.fleetdatasource.jdbcUrl=jdbc:postgresql://dev-pg-logistics1.dc.ifood.com.br:5432/ifood-fleet
spring.fleetdatasource.username=myusername
spring.fleetdatasource.password=mypassword

spring.areadatasource.jdbcUrl=jdbc:postgresql://dev-pg-logistics1.dc.ifood.com.br:5432/logistic-area
spring.areadatasource.username=myusername
spring.areadatasource.password=mypassword
```

### Tenant Identify

Once the tenant depends on datasource you have got to change the tenant to the correct one in the properties

```
source.tenant=MX
```

### Running the application

The application can be started as any Spring Boot app: `java -jar package.jar` or `mvn spring-boot:run`.
As explained above, running it without parameters is the same as calling the following command

```
java -jar package.jar
```
or 
```
mvn spring-boot:run
```

### Running locally

To run this application locally, it's necessary to start Localstack for kml bucket.
Vide https://bitbucket.org/ifood/ifood-delivery-platform-local-environment.

Then, the application can be executed by calling `mvn spring-boot:run -Dspring.profiles.active=localstack`.

## Specific S3 commands

##### Create a bucket
```
aws --endpoint-url=http://localhost:4572 s3 mb s3://development-ifood-fleet-worker-telemetry
```
##### List buckets
```
aws --endpoint-url=http://localhost:4572 s3 ls
```

##### Copy a file over
```
aws --endpoint-url=http://localhost:4572 s3 cp /tmp/mongo.log s3://development-ifood-fleet-worker-telemetry
```


##### List files
```
aws --endpoint-url=http://localhost:4572 s3 ls s3://development-ifood-fleet-worker-telemetry

aws --endpoint-url=http://localhost:4572 s3 ls s3://development-ifood-fleet-worker-telemetry/br/

aws --endpoint-url=http://localhost:4572 s3 ls s3://development-ifood-fleet-worker-telemetry/br/orders/

aws --endpoint-url=http://localhost:4572 s3 ls s3://development-ifood-fleet-worker-telemetry/br/routes/

```

##### Delete this fileg
```
aws --endpoint-url=http://localhost:4572 s3 rm s3://development-ifood-fleet-worker-telemetry/mongo.log
```
##### Specific

```
aws --endpoint-url=http://localhost:4572 s3 cp São\ Paulo\ \(1\).kml s3://delivery-data.ifood.com.br/attributes/FILE/SUBSET/entity-id-1/attribute-key-KML_FILE_PATH/São\ Paulo\ \(1\).kml
aws --endpoint-url=http://localhost:4572 s3 ls s3://delivery-data.ifood.com.br
```

