package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;

public interface SubSetService {
    List<SubSetDTO> getByType(SubSetType subSetType);
    List<SubSetDTO> getChildrenParent(Long parent);
}
