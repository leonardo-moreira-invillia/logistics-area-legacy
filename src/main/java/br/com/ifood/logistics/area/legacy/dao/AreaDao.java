package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.AreaDTO;

public interface AreaDao {
    void insert(AreaDTO areaDTO);
    List<AreaDTO>  getAreas();
    AreaDTO createFromMap(Map<String, Object> map);
}
