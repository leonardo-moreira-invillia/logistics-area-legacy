package br.com.ifood.logistics.area.legacy.dao;

public interface AreaAttributeValueDao {
    void insert(String areaUUID, String attributeUUID, String value);
}
