package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.model.AreaDTO;
import br.com.ifood.logistics.area.legacy.model.AttributeDomainDTO;

public interface AreaService {
    void insert(AreaDTO areaDTO);
    List<AreaDTO>  getAreas();
}
