package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import br.com.ifood.logistics.area.legacy.model.AreaDTO;
import br.com.ifood.logistics.area.legacy.model.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository
public class AreaDaoImpl implements AreaDao {

    @Autowired
    Map<Source, JdbcTemplate> templateMap;

    @Override
    public void insert(AreaDTO areaDTO) {
        int result = templateMap.get(Source.AREA).queryForObject("SELECT COUNT(*) FROM area where uuid = ?", new Object[] {areaDTO.getUuid()}, Integer.class);

        if(result == 0){
            if(!StringUtils.isEmpty(areaDTO.getGeo()))
                templateMap.get(Source.AREA).update("insert into area values (?,?,?,?,?,  ST_Force2D(ST_MakeValid(ST_GeomFromKML(?))), ?, now(), now(), ?)", areaDTO.getUuid(), areaDTO.getName(), areaDTO.getKmlUuid(), areaDTO.getKmlPath(), areaDTO.getKmlContent(), areaDTO.getGeo(), areaDTO.getDescription(), areaDTO.isTest());
            else
                templateMap.get(Source.AREA).update("insert into area values (?,?,?,?,?, null, ?, now(), now(), ?)", areaDTO.getUuid(), areaDTO.getName(), areaDTO.getKmlUuid(), areaDTO.getKmlPath(), areaDTO.getKmlContent(), areaDTO.getDescription(), areaDTO.isTest());
        }
    }

    @Override
    public List<AreaDTO> getAreas() {
        List<Map<String, Object>> rows = templateMap.get(Source.AREA).queryForList("Select * from AREA", new Object[] {});
        List<AreaDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }

    @Override
    public AreaDTO createFromMap(Map<String, Object> map) {
        AreaDTO dto = new AreaDTO();
        dto.setUuid(map.get("uuid").toString());
        dto.setName(map.get("name").toString());

        if(map.get("kml_uuid") != null)
            dto.setKmlUuid(map.get("kml_uuid").toString());

        if(map.get("kml_path") != null)
            dto.setKmlPath(map.get("kml_path").toString());

        return dto;
    }

}
