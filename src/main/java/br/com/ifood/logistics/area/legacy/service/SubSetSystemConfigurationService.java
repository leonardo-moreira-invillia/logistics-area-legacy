package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;

public interface SubSetSystemConfigurationService {
    List<SubSetAttributeDTO> getSystemConfigurationBySubSetUUID(String uuid);
}
