package br.com.ifood.logistics.area.legacy.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AreaDTO {
    private String uuid;
    private String name;
    private String kmlUuid;
    private String kmlPath;
    private String kmlContent;
    private String geo;
    private String description;
    private boolean isTest;
    private String tenant;
    private SubSetType type;
    private List<AreaDTO> subs;
}
