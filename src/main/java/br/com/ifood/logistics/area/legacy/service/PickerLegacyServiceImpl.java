package br.com.ifood.logistics.area.legacy.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import br.com.ifood.logistics.area.legacy.model.AreaDTO;
import br.com.ifood.logistics.area.legacy.model.AttributeName;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;
import br.com.ifood.logistics.area.legacy.repository.S3RepositoryService;
import br.com.ifood.logistics.area.legacy.utils.KmlUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("pickerLegacyServiceImpl")
@Slf4j
public class PickerLegacyServiceImpl implements LegacyService {

    SubSetService subSetService;
    SubSetAttributeService subSetAttributeService;
    ObjectMapper jacksonObjectMapper;
    S3RepositoryService s3RepositoryService;
    String tenant;

    @Autowired
    public PickerLegacyServiceImpl(final ObjectMapper jacksonObjectMapper,
                                   final SubSetService subSetService,
                                   final SubSetAttributeService subSetAttributeService,
                                   final S3RepositoryService s3RepositoryService,
                                   @Value("${source.tenant}") final String tenant) {
        this.subSetService = subSetService;
        this.subSetAttributeService = subSetAttributeService;
        this.jacksonObjectMapper = jacksonObjectMapper;
        this.s3RepositoryService = s3RepositoryService;
        this.tenant = tenant;
    }

    @Override
    public void consolidate() {
        List<SubSetDTO> subSets = subSetService.getByType(SubSetType.PICKER_REGION);
        List<AreaDTO> regions = new ArrayList<>();

        for(SubSetDTO subSet : subSets){
            AreaDTO region = new AreaDTO();
            region.setUuid(subSet.getUuid());
            region.setName(subSet.getName());
            region.setKmlUuid(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(subSet.getId()), AttributeName.KML_FILE_ID.toString()).getValue());
            region.setKmlPath(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(subSet.getId()), AttributeName.KML_FILE_PATH.toString()).getValue());
            region.setKmlContent(s3RepositoryService.getFile(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(subSet.getId()), AttributeName.KML_FILE_PATH.toString()).getValue()));
            region.setType(SubSetType.PICKER_REGION);
            region.setTenant(tenant);
            if(region.getKmlContent() != null)
                region.setGeo(KmlUtils.toSingleGeometryElement(region.getKmlContent()));

            regions.add(region);
        }

        System.out.println("PICKERS");
        System.out.println("DO THE INSERT");

        try {
            System.out.println(jacksonObjectMapper.writeValueAsString(regions));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

}
