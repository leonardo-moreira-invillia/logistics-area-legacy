package br.com.ifood.logistics.area.legacy.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import br.com.ifood.logistics.area.legacy.model.AttributeDomainDTO;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("domainAttributeLegacyServiceImpl")
@Slf4j
public class DomainAttributeLegacyServiceImpl implements LegacyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DomainAttributeLegacyServiceImpl.class);

    DomainAttributeService domainAttributeService;
    ObjectMapper jacksonObjectMapper;

    @Autowired
    public DomainAttributeLegacyServiceImpl(final ObjectMapper jacksonObjectMapper,
                                            final DomainAttributeService domainAttributeService) {
        this.domainAttributeService = domainAttributeService;
        this.jacksonObjectMapper = jacksonObjectMapper;
    }

    @Override
    public void consolidate() {
        LOGGER.info("START OF DOMAIN ATTRIBUTES");

        List<AttributeDomainDTO> dtoList = domainAttributeService.getAttributeDomain();

        for(AttributeDomainDTO dto : dtoList){

            if(("KML_FILE_PATH".equals(dto.getName())) || ("KML_FILE_ID".equals(dto.getName())))
                continue;

            domainAttributeService.insert(dto);
        }

        // Added after, kept appart because of dev
        /// depois eu vejo isso...

        List<AttributeDomainDTO> dtoListConfiguration = domainAttributeService.getAttributeDomainConfiguration();

        for(AttributeDomainDTO dto : dtoListConfiguration){
            domainAttributeService.insert(dto);
        }

        LOGGER.info("END OF DOMAIN ATTRIBUTES");

    }

}
