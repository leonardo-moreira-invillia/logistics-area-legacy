package br.com.ifood.logistics.area.legacy.dao;

import br.com.ifood.logistics.area.legacy.model.Source;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class SubSetDaoImpl implements SubSetDao {

    @Autowired
    Map<Source, JdbcTemplate> templateMap;
    private final String querySubSet = "select * from subset where partitions_name = ? order by id";
    private final String querySubSetSubSet = "select subset.* from subset_subset  inner join subset on subset.id = subset_subset.subset_id1 where subset_id2 = ?";

    @Override
    public SubSetDTO createFromMap(Map<String, Object> map) {
        SubSetDTO dto = new SubSetDTO();
        dto.setId(map.get("id").toString());
        dto.setUuid(map.get("uuid").toString());
        dto.setName(map.get("name").toString());

        if(map.get("is_test") != null)
            dto.setTest(Boolean.parseBoolean(map.get("is_test").toString()));

        if(map.get("description") != null)
            dto.setDescription(map.get("description").toString());
        dto.setType(map.get("partitions_name").toString());
        return dto;
    }

    @Override
    public List<SubSetDTO> getByType(SubSetType subSetType) {
        List<Map<String, Object>> rows = templateMap.get(Source.FLEET).queryForList(querySubSet, new Object[] {subSetType.toString()});
        List<SubSetDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }

    @Override
    public List<SubSetDTO> getChildrenParent(Long parent) {
        List<Map<String, Object>> rows = templateMap.get(Source.FLEET).queryForList(querySubSetSubSet, new Object[] {parent});
        List<SubSetDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }
}
