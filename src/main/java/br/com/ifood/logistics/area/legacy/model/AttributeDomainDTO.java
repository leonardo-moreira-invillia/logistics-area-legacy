package br.com.ifood.logistics.area.legacy.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttributeDomainDTO {
    private String uuid;
    private String domain;
    private String name;
    private String description;
    private String attributeType;
    private String areaType;
}
