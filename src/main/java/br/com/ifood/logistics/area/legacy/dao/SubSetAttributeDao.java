package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;

public interface SubSetAttributeDao {
    SubSetAttributeDTO getBySubSetIdAndAttributeKeyName(Long subsetId, String attributeKeyName);
    List<SubSetAttributeDTO> getBySubSetId(Long subsetId);
    SubSetAttributeDTO createFromMap(Map<String, Object> map);
    List<SubSetAttributeDTO> getBySubSetAttributesBySubSetUUID(String uuid);

}
