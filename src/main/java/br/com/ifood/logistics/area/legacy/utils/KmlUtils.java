package br.com.ifood.logistics.area.legacy.utils;


import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public final class KmlUtils {

    private static final String PLACEMARK_TAG_NAME = "Placemark";

    private static final String MULTI_GEOMETRY_TAG_NAME = "MultiGeometry";

    private static final String POLYGON_TAG_NAME = "Polygon";

    private KmlUtils() {
        throw new AssertionError("You shouldn't do that");
    }

    /**
     * Converts the given kml to the its document name.
     *
     * @param kml the content of a kml file
     * @return the document name
     */
    public static String toDocumentName(final String kml) {
        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.parse(new InputSource(new StringReader(kml)));
            return parseElementName(document.getDocumentElement());
        } catch (final Exception e) {
            throw new RuntimeException("Couldn't parse kml file: " + kml, e);
        }
    }

    /**
     * Converts the given kml to a single geometry element.
     *
     * @param kml the content of a kml file
     * @return a single geometry element
     */
    public static String toSingleGeometryElement(final String kml) {
        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder builder = factory.newDocumentBuilder();
            final Document document = builder.parse(new InputSource(new StringReader(kml)));

            final ImmutableSet.Builder<String> polygonElements = ImmutableSet.builder();
            final NodeList placemarks = document.getDocumentElement().getElementsByTagName(PLACEMARK_TAG_NAME);
            for (int index = 0; index < placemarks.getLength(); index++) {
                final Node node = placemarks.item(index);
                if (!(node instanceof Element)) {
                    continue;
                }
                final Element placemark = (Element) node;
                final List<String> geometryElements = parseGeometryElement(placemark);
                for (final String geometryElement : geometryElements) {
                    polygonElements.add(geometryElement);
                }
            }
            final Set<String> result = polygonElements.build();
            checkArgument(!result.isEmpty());

            if (result.size() == 1) {
                return Iterables.getOnlyElement(result);
            }

            final StringBuilder multiGeometryElementBuilder = new StringBuilder("<").append(MULTI_GEOMETRY_TAG_NAME).append(">");
            result.forEach(multiGeometryElementBuilder::append);
            return multiGeometryElementBuilder
                    .append("</").append(MULTI_GEOMETRY_TAG_NAME).append(">")
                    .toString();
        } catch (final Exception e) {
            throw new RuntimeException("Couldn't parse kml file: " + kml, e);
        }
    }

    private static String parseElementName(final Element element) {
        final NodeList nodeList = element.getElementsByTagName("name");
        if (nodeList.getLength() > 0) {
            return nodeList.item(0).getTextContent();
        }
        return new RandomStringGenerator.Builder().filteredBy(CharacterPredicates.LETTERS).build().generate(5);
    }

    private static List<String> parseGeometryElement(final Element placemark) throws TransformerException {
        final NodeList geometryList = placemark.getElementsByTagName("Polygon");
        final List<String> polygons = new ArrayList<>();
        for (int index = 0; index < geometryList.getLength(); index++) {
            polygons.add(toString(geometryList.item(index)));
        }
        if (polygons.isEmpty()) {
            throw new IllegalArgumentException("No geometry defined in placemark element");
        }
        return polygons;
    }

    private static String toString(final Node node) throws TransformerException {
        final Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        final StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(node), new StreamResult(writer));
        return writer.toString();
    }
}
