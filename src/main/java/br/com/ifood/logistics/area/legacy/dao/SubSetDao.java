package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;

public interface SubSetDao {
    List<SubSetDTO> getByType(SubSetType subSetType);
    List<SubSetDTO> getChildrenParent(Long parent);
    SubSetDTO createFromMap(Map<String, Object> map);
}
