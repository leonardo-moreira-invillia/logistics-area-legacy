package br.com.ifood.logistics.area.legacy.service;

public interface RegionService {
    void insert(String uuid, String tenant);
}
