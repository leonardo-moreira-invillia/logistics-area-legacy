package br.com.ifood.logistics.area.legacy.service;

public interface AreaAttributeValueService {
    void insert(String areaUUID, String attributeUUID, String value);
}
