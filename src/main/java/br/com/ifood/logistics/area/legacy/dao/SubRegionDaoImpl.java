package br.com.ifood.logistics.area.legacy.dao;

import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SubRegionDaoImpl implements SubRegionDao {

    @Autowired
    Map<Source, JdbcTemplate> templateMap;

    @Override
    public void insert(String uuid, String regionUuid, String type) {
        int result = templateMap.get(Source.AREA).queryForObject("SELECT COUNT(*) FROM sub_region where uuid = ? and region_uuid = ? ", new Object[] {uuid, regionUuid}, Integer.class);

        if(result == 0){
            templateMap.get(Source.AREA).update("insert into sub_region values (?,?,?)", uuid, regionUuid, type);
        }
    }
}
