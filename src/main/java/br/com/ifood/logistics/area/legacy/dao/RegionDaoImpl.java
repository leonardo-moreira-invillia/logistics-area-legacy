package br.com.ifood.logistics.area.legacy.dao;

import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class RegionDaoImpl implements RegionDao {

    @Autowired
    Map<Source, JdbcTemplate> templateMap;

    @Override
    public void insert(String uuid, String tenant) {
        int result = templateMap.get(Source.AREA).queryForObject("SELECT COUNT(*) FROM region where uuid = ?", new Object[] {uuid}, Integer.class);

        if(result == 0){
            templateMap.get(Source.AREA).update("insert into region values (?,?)", uuid, tenant);
        }

    }
}
