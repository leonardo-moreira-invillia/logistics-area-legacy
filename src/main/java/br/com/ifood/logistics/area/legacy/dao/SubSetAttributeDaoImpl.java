package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import br.com.ifood.logistics.area.legacy.model.Source;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SubSetAttributeDaoImpl implements SubSetAttributeDao {

    @Autowired
    Map<Source, JdbcTemplate> templateMap;
    private final String querySubSetIdAndKeyName = "select * from subset_attribute where subset_id = ? and attributes_key_name = ?";
    private final String querySubSetId = "select * from subset_attribute where subset_id = ? ";
    private final String queryAllSubSeAttributesBySubSetUUID = "select b.uuid, b.partitions_name, 'FLEET_ATTRIBUTE' as domain,  a.* from subset_attribute a inner join subset b on b.id = a.subset_id where b.uuid::text = ?";

    @Override
    public SubSetAttributeDTO getBySubSetIdAndAttributeKeyName(Long subsetId, String attributeKeyName) {
        List<Map<String, Object>> rows = templateMap.get(Source.FLEET).queryForList(querySubSetIdAndKeyName, new Object[] {subsetId, attributeKeyName});
        List<SubSetAttributeDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList.size() == 1 ? dtoList.get(0) : new SubSetAttributeDTO();
    }

    @Override
    public List<SubSetAttributeDTO> getBySubSetId(Long subsetId) {
        List<Map<String, Object>> rows = templateMap.get(Source.FLEET).queryForList(querySubSetId, new Object[] {subsetId});
        List<SubSetAttributeDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }

    @Override
    public SubSetAttributeDTO createFromMap(Map<String, Object> map) {
        SubSetAttributeDTO dto = new SubSetAttributeDTO();
        dto.setKey(map.get("attributes_key_name").toString());
        dto.setValue(map.get("value").toString());

        if(map.get("domain") != null)
            dto.setDomain(map.get("domain").toString());

        if(map.get("partitions_name") != null)
            dto.setAreaType(SubSetType.valueOf(map.get("partitions_name").toString()).getAreaType());

        return dto;
    }

    @Override
    public List<SubSetAttributeDTO> getBySubSetAttributesBySubSetUUID(String uuid) {
        List<Map<String, Object>> rows = templateMap.get(Source.FLEET).queryForList(queryAllSubSeAttributesBySubSetUUID, new Object[] {uuid});
        List<SubSetAttributeDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }

}
