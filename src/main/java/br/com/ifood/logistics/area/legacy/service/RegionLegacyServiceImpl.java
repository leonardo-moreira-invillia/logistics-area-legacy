package br.com.ifood.logistics.area.legacy.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import br.com.ifood.logistics.area.legacy.model.AreaDTO;
import br.com.ifood.logistics.area.legacy.model.AttributeDomainDTO;
import br.com.ifood.logistics.area.legacy.model.AttributeName;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;
import br.com.ifood.logistics.area.legacy.repository.S3RepositoryService;
import br.com.ifood.logistics.area.legacy.utils.KmlUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("regionLegacyServiceImpl")
@Slf4j
public class RegionLegacyServiceImpl implements LegacyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegionLegacyServiceImpl.class);

    AreaService areaService;
    AreaAttributeValueService areaAttributeValueService;
    DomainAttributeService domainAttributeService;
    SubSetSystemConfigurationService subSetSystemConfigurationService;
    RegionService regionService;
    SubRegionService subRegionService;
    SubSetService subSetService;
    SubSetAttributeService subSetAttributeService;
    ObjectMapper jacksonObjectMapper;
    S3RepositoryService s3RepositoryService;
    String tenant;

    @Autowired
    public RegionLegacyServiceImpl(final ObjectMapper jacksonObjectMapper,
                                   final SubSetService subSetService,
                                   final AreaService areaService,
                                   final AreaAttributeValueService areaAttributeValueService,
                                   final SubSetSystemConfigurationService subSetSystemConfigurationService,
                                   final DomainAttributeService domainAttributeService,
                                   final RegionService regionService,
                                   final SubRegionService subRegionService,
                                   final SubSetAttributeService subSetAttributeService,
                                   final S3RepositoryService s3RepositoryService,
                                   @Value("${source.tenant}") final String tenant) {
        this.subSetService = subSetService;
        this.subSetAttributeService = subSetAttributeService;
        this.areaAttributeValueService = areaAttributeValueService;
        this.jacksonObjectMapper = jacksonObjectMapper;
        this.areaService = areaService;
        this.subSetSystemConfigurationService = subSetSystemConfigurationService;
        this.domainAttributeService = domainAttributeService;
        this.subRegionService = subRegionService;
        this.regionService = regionService;
        this.s3RepositoryService = s3RepositoryService;
        this.tenant = tenant;
    }

    @Override
    public void consolidate() {
        LOGGER.info("******************START OF REGIONS*******************");


        List<SubSetDTO> subSets = subSetService.getByType(SubSetType.REGIONS);
        List<AreaDTO> regions = new ArrayList<>();

        for(SubSetDTO subSet : subSets){
            AreaDTO region = new AreaDTO();
            region.setUuid(subSet.getUuid());
            region.setName(subSet.getName());
            region.setTest(subSet.isTest());
            region.setKmlUuid(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(subSet.getId()), AttributeName.KML_FILE_ID.toString()).getValue());
            region.setKmlPath(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(subSet.getId()), AttributeName.KML_FILE_PATH.toString()).getValue());
            region.setKmlContent(s3RepositoryService.getFile(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(subSet.getId()), AttributeName.KML_FILE_PATH.toString()).getValue()));
            region.setType(SubSetType.REGIONS);
            region.setTenant(tenant);

            if(region.getKmlContent() != null) {
                try {
                    region.setGeo(KmlUtils.toSingleGeometryElement(region.getKmlContent()));
                } catch (Exception ex) {
                    LOGGER.error("Error on parse S3 KML-CONTENT: "+ ex.getMessage());
                }
            }

            //Deal with SubRegion Related despite de type

            List<SubSetDTO> suns = subSetService.getChildrenParent(Long.valueOf(subSet.getId()));
            List<AreaDTO> subRegions = new ArrayList<>();

            for(SubSetDTO sun : suns) {
                AreaDTO subregion = new AreaDTO();
                subregion.setUuid(sun.getUuid());
                subregion.setName(sun.getName());
                subregion.setTest(sun.isTest());
                subregion.setKmlUuid(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(sun.getId()), AttributeName.KML_FILE_ID.toString()).getValue());
                subregion.setKmlPath(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(sun.getId()), AttributeName.KML_FILE_PATH.toString()).getValue());
                subregion.setKmlContent(s3RepositoryService.getFile(subSetAttributeService.getBySubSetIdAndAttributeKeyName(Long.valueOf(sun.getId()), AttributeName.KML_FILE_PATH.toString()).getValue()));
                subregion.setType(SubSetType.valueOf(sun.getType()));
                subregion.setTenant(tenant);
                if(subregion.getKmlContent() != null)
                    subregion.setGeo(KmlUtils.toSingleGeometryElement(subregion.getKmlContent()));
                subRegions.add(subregion);
            }

            region.setSubs(subRegions);
            regions.add(region);
        }

        LOGGER.info("START OF AREA TABLE OF REGIONS");

        for(AreaDTO area : regions){
            areaService.insert(area);
            regionService.insert(area.getUuid(), area.getTenant());


            LOGGER.info("START OF AREA TABLE OF SUBREGIONS");

            for(AreaDTO sub : area.getSubs()) {
                areaService.insert(sub);
                subRegionService.insert(sub.getUuid(), area.getUuid(), sub.getType().getAreaType());
            }

            LOGGER.info("END OF AREA TABLE OF SUBREGIONS");

        }

        LOGGER.info("END OF AREA TABLE OF REGIONS");

        LOGGER.info("******************END OF REGIONS*******************");


        LOGGER.info("START OF AREA ATTRIBUTES");

        List<AreaDTO> areas = areaService.getAreas();

        for(AreaDTO areaDTO : areas){

            List<SubSetAttributeDTO> attributesList = subSetAttributeService.getBySubSetAttributesBySubSetUUID(areaDTO.getUuid());

            for(SubSetAttributeDTO attributeDTO : attributesList){
                AttributeDomainDTO attributeDomainDTO = domainAttributeService.getAttributeDomainByDomainKeyAreaType(attributeDTO.getDomain(), attributeDTO.getKey(), attributeDTO.getAreaType());

                if(attributeDomainDTO != null) {
                    areaAttributeValueService.insert(areaDTO.getUuid(), attributeDomainDTO.getUuid(), attributeDTO.getValue());
                } else {
                    if (("KML_FILE_PATH".equals(attributeDTO.getKey())) || ("KML_FILE_ID".equals(attributeDTO.getKey())))
                        continue;

                    LOGGER.error("Problem on attribute of: " + areaDTO.getUuid());
                }
            }
        }

        LOGGER.info("END OF AREA ATTRIBUTES");

        //CONFIRMAR ISSO...

        LOGGER.info("START OF CONFIG ATTRIBUTES");

        List<AreaDTO> areasConfiguration = areaService.getAreas();

        for(AreaDTO areaDTO : areasConfiguration){

            List<SubSetAttributeDTO> attributesList = subSetSystemConfigurationService.getSystemConfigurationBySubSetUUID(areaDTO.getUuid());

            for(SubSetAttributeDTO attributeDTO : attributesList){
                AttributeDomainDTO attributeDomainDTO = domainAttributeService.getAttributeDomainByDomainKeyAreaType(attributeDTO.getDomain(), attributeDTO.getKey(), attributeDTO.getAreaType());

                if(attributeDomainDTO != null)
                    areaAttributeValueService.insert(areaDTO.getUuid(), attributeDomainDTO.getUuid(), attributeDTO.getValue());
                else
                    LOGGER.error("Problem on attribute of: "+ areaDTO.getUuid());
            }
        }

        LOGGER.info("END OF AREA ATTRIBUTES");








    }

}
