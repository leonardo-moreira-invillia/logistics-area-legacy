package br.com.ifood.logistics.area.legacy.dao;

public interface RegionDao {
    void insert(String uuid, String tenant);
}
