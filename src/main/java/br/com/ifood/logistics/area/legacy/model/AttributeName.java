package br.com.ifood.logistics.area.legacy.model;

public enum AttributeName {
    KML_FILE_PATH, KML_FILE_ID
}
