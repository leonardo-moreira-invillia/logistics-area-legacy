package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.dao.SubSetSystemConfigurationDao;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("subSetSystemConfigurationServiceImpl")
@Slf4j
public class SubSetSystemConfigurationServiceImpl implements SubSetSystemConfigurationService {

    @Autowired
    private SubSetSystemConfigurationDao subSetSystemConfigurationDao;

    @Override
    public List<SubSetAttributeDTO> getSystemConfigurationBySubSetUUID(String uuid) {
        return subSetSystemConfigurationDao.getSystemConfigurationBySubSetUUID(uuid);
    }
}
