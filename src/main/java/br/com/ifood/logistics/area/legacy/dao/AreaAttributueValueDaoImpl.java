package br.com.ifood.logistics.area.legacy.dao;

import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AreaAttributueValueDaoImpl implements AreaAttributeValueDao {

    @Autowired
    Map<Source, JdbcTemplate> templateMap;

    @Override
    public void insert(String areaUUID, String attributeUUID, String value) {
        templateMap.get(Source.AREA).update("insert into area_attribute_value values (?,?,?)", areaUUID, attributeUUID, value);
    }
}
