package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.AttributeDomainDTO;

public interface DomainAttributeDao {
    List<AttributeDomainDTO> getAttributeDomain();
    List<AttributeDomainDTO> getAttributeDomainConfiguration();
    AttributeDomainDTO getAttributeDomainByDomainKeyAreaType(String domain, String key, String areaType);
    void insert(AttributeDomainDTO attributeDomain);
    AttributeDomainDTO createFromMap(Map<String, Object> map);
    AttributeDomainDTO createFromMapArea(Map<String, Object> map);
}
