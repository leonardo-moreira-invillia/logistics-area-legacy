package br.com.ifood.logistics.area.legacy.model;

public enum SubSetType {
    REGIONS("REGION"),
    SUB_REGIONS("SUB_REGION"),
    LOCKER_REGIONS("LOCKER"),
    PICKER_REGION("PICKER"),
    DOMAIN_ATTRIBUTE("DOMAIN");

    private String areaType;

    SubSetType(String areaType){
        this.areaType = areaType;
    }

    public String getAreaType() {
        return areaType;
    }
}
