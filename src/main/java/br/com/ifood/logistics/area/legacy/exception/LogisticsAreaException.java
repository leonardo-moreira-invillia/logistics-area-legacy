package br.com.ifood.logistics.area.legacy.exception;

public class LogisticsAreaException extends RuntimeException {

    public LogisticsAreaException(String message) {
        super(message);
    }

    public LogisticsAreaException(final Exception e) {
        super(e);
    }
}
