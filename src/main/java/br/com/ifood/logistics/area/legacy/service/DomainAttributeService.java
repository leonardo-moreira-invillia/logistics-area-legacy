package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.model.AttributeDomainDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;

public interface DomainAttributeService {
    List<AttributeDomainDTO> getAttributeDomainConfiguration();
    List<AttributeDomainDTO> getAttributeDomain();
    void insert(AttributeDomainDTO attributeDomain);
    AttributeDomainDTO getAttributeDomainByDomainKeyAreaType(String domain, String key, String areaType);
}
