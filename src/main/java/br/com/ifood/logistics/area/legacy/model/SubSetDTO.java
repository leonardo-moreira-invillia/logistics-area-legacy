package br.com.ifood.logistics.area.legacy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubSetDTO {

    private String id;
    private String uuid;
    private String name;
    private String description;
    private String type;
    private boolean isTest;

}
