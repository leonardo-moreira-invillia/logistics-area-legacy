package br.com.ifood.logistics.area.legacy.service;

import br.com.ifood.logistics.area.legacy.dao.AreaAttributeValueDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("areaAttributeValueServiceImpl")
@Slf4j
public class AreaAttributeValueServiceImpl implements AreaAttributeValueService {

    @Autowired
    private AreaAttributeValueDao areaAttributeValueDao;

    @Override
    public void insert(String areaUUID, String attributeUUID, String value) {
        areaAttributeValueDao.insert(areaUUID, attributeUUID, value);
    }
}
