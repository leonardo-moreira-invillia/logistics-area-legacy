package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.dao.AreaDao;
import br.com.ifood.logistics.area.legacy.model.AreaDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("areaServiceImpl")
@Slf4j
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaDao areaDao;

    @Override
    public void insert(AreaDTO areaDTO) {
        areaDao.insert(areaDTO);
    }

    @Override
    public List<AreaDTO> getAreas() {
        return areaDao.getAreas();
    }
}
