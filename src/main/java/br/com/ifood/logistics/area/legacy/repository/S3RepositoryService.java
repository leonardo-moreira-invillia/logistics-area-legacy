package br.com.ifood.logistics.area.legacy.repository;

public interface S3RepositoryService {
    String getFile(String path);
    boolean doesObjectExists(String path);
}
