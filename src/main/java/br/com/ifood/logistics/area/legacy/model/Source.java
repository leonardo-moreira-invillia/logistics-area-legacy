package br.com.ifood.logistics.area.legacy.model;

public enum Source {
    FLEET, AREA
}
