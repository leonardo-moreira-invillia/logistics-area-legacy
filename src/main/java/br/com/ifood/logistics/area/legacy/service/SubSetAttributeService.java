package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;

public interface SubSetAttributeService {
    SubSetAttributeDTO getBySubSetIdAndAttributeKeyName(Long subsetId, String attributeKeyName);
    List<SubSetAttributeDTO> getBySubSetId(Long subsetId);
    List<SubSetAttributeDTO> getBySubSetAttributesBySubSetUUID(String uuid);
}
