package br.com.ifood.logistics.area.legacy.config;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import br.com.ifood.logistics.area.legacy.model.Source;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@ComponentScan("br.com.ifood.logistics.area.legacy")
@EnableConfigurationProperties
public class AppConfig {

    @Bean("fleetDataSource")
    @ConfigurationProperties(prefix="spring.fleetdatasource")
    public DataSource fleetDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean("areaDataSource")
    @ConfigurationProperties(prefix="spring.areadatasource")
    public DataSource areaDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean("fleetJdbcTemplate")
    public JdbcTemplate fleetJdbcTemplate(DataSource fleetDataSource) {
        return new JdbcTemplate(fleetDataSource);
    }

    @Bean("areaJdbcTemplate")
    public JdbcTemplate areaJdbcTemplate(DataSource areaDataSource) {
        return new JdbcTemplate(areaDataSource);
    }

    @Bean("templateMap")
    public Map<Source, JdbcTemplate> jdbcTemplateMap(JdbcTemplate fleetJdbcTemplate, JdbcTemplate areaJdbcTemplate) {
        Map<Source, JdbcTemplate> jdbcTemplateMap = new HashMap<>();
        jdbcTemplateMap.put(Source.FLEET, fleetJdbcTemplate);
        jdbcTemplateMap.put(Source.AREA, areaJdbcTemplate);
        return jdbcTemplateMap;
    }

}
