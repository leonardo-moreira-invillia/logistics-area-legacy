package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import br.com.ifood.logistics.area.legacy.model.AttributeDomainDTO;
import br.com.ifood.logistics.area.legacy.model.Source;
import br.com.ifood.logistics.area.legacy.model.SubSetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class DomainAttributeDaoImpl implements DomainAttributeDao {

    @Autowired
    Map<Source, JdbcTemplate> templateMap;
    private final String query = "select distinct a.uuid, 'FLEET_ATTRIBUTE' as domain, a.\"name\", a.description, b.attribute_type, d.partitions_name from attribute_key a \n" +
            "inner join attribute_validator b on a.attribute_validator_id = b.id \n" +
            "inner join subset_attribute c on a.id = c.attribute_key_id\n" +
            "inner join subset d on d.id = c.subset_id\n" +
            "where d.partitions_name in ('REGIONS', 'SUB_REGIONS', 'LOCKER_REGIONS', 'PICKER_REGION')\n" +
            "order by \"name\"";

    private final String queryConfig = "select distinct 'UUID' as uuid, 'FLEET_CONFIG' as domain, key as name, '' as description, 'STRING' as attribute_type, d.partitions_name from system_configuration_by_subset a \n" +
            "inner join subset d on d.id = a.subset_id\n" +
            "where d.partitions_name in ('REGIONS', 'SUB_REGIONS', 'LOCKER_REGIONS', 'PICKER_REGION')\n" +
            "order by name\n";

    @Override
    public AttributeDomainDTO createFromMap(Map<String, Object> map) {
        AttributeDomainDTO dto = new AttributeDomainDTO();
        dto.setUuid(UUID.randomUUID().toString());
        dto.setDomain(map.get("domain").toString().trim());
        dto.setName(map.get("name").toString().trim());

        if(map.get("description") != null)
            dto.setDescription(map.get("description").toString().trim());

        dto.setAttributeType( map.get("attribute_type") == null ? "REGEX_ATTRIBUTE_VALIDATOR" : map.get("attribute_type").toString().trim());
        dto.setAreaType(SubSetType.valueOf(map.get("partitions_name").toString()).getAreaType());
        return dto;
    }

    @Override
    public AttributeDomainDTO createFromMapArea(Map<String, Object> map) {
        AttributeDomainDTO dto = new AttributeDomainDTO();
        dto.setUuid(map.get("uuid").toString());
        dto.setDomain(map.get("domain").toString());

        if(map.get("key") != null)
            dto.setName(map.get("key").toString().trim());

        if(map.get("description") != null)
            dto.setDescription(map.get("description").toString().trim());

        dto.setAttributeType(map.get("type").toString());
        dto.setAreaType(map.get("area_type").toString());
        return dto;
    }

    @Override
    public List<AttributeDomainDTO> getAttributeDomain() {
        List<Map<String, Object>> rows = templateMap.get(Source.FLEET).queryForList(query, new Object[] {});
        List<AttributeDomainDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }

    @Override
    public List<AttributeDomainDTO> getAttributeDomainConfiguration() {
        List<Map<String, Object>> rows = templateMap.get(Source.FLEET).queryForList(queryConfig, new Object[] {});
        List<AttributeDomainDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }

    @Override
    public AttributeDomainDTO getAttributeDomainByDomainKeyAreaType(String domain, String key, String areaType) {
        List<Map<String, Object>> rows = templateMap.get(Source.AREA).queryForList("select * from domain_attribute where domain = ? and key = ? and area_type = ?", new Object[] {domain, key, areaType});
        List<AttributeDomainDTO> dtoList = rows.stream().map(item -> this.createFromMapArea(item)).collect(Collectors.toList());

        if(dtoList.size() == 0){
            return null;
        } else {
            return dtoList.get(0);
        }
    }

    @Override
    public void insert(AttributeDomainDTO attributeDomain) {
        int result = templateMap.get(Source.AREA).queryForObject("SELECT COUNT(*) FROM domain_attribute where domain = ? and key = ? and type = ? and area_type = ?", new Object[] {attributeDomain.getDomain(), attributeDomain.getName(), attributeDomain.getAttributeType(), attributeDomain.getAreaType()}, Integer.class);

        if(result == 0)
            templateMap.get(Source.AREA).update("insert into domain_attribute values (?,?,?,?,?,?)", attributeDomain.getUuid(), attributeDomain.getDomain(), attributeDomain.getName(), attributeDomain.getAttributeType(), attributeDomain.getAreaType(), attributeDomain.getDescription());
    }

}
