package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.dao.SubSetDao;
import br.com.ifood.logistics.area.legacy.model.SubSetDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("subSetService")
@Slf4j
public class SubSetServiceImpl implements SubSetService {

    @Autowired
    private SubSetDao subSetDao;

    @Override
    public List<SubSetDTO> getByType(SubSetType subSetType) {
        return subSetDao.getByType(subSetType);
    }

    @Override
    public List<SubSetDTO> getChildrenParent(Long parent) {
        return subSetDao.getChildrenParent(parent);
    }
}
