package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.dao.DomainAttributeDao;
import br.com.ifood.logistics.area.legacy.model.AttributeDomainDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("domainAttributeServiceImpl")
@Slf4j
public class DomainAttributeServiceImpl implements DomainAttributeService {

    @Autowired
    private DomainAttributeDao domainAttributeDao;

    @Override
    public List<AttributeDomainDTO> getAttributeDomain() {
        return domainAttributeDao.getAttributeDomain();
    }

    @Override
    public List<AttributeDomainDTO> getAttributeDomainConfiguration() {
        return domainAttributeDao.getAttributeDomainConfiguration();
    }

    @Override
    public void insert(AttributeDomainDTO attributeDomain) {
        domainAttributeDao.insert(attributeDomain);
    }

    @Override
    public AttributeDomainDTO getAttributeDomainByDomainKeyAreaType(String domain, String key, String areaType) {
        return domainAttributeDao.getAttributeDomainByDomainKeyAreaType(domain, key, areaType);
    }
}
