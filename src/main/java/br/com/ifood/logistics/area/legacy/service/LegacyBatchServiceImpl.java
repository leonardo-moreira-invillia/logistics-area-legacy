package br.com.ifood.logistics.area.legacy.service;

import java.util.HashMap;
import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.SubSetType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("legacyBatchServiceImpl")
@Slf4j
public class LegacyBatchServiceImpl implements LegacyBatchService {

    private Map<SubSetType, LegacyService> legacyServices = new HashMap<>();

    @Autowired
    public LegacyBatchServiceImpl(@Qualifier("regionLegacyServiceImpl") final LegacyService regionLegacyServiceImpl,
                                  @Qualifier("domainAttributeLegacyServiceImpl") final LegacyService domainAttributeLegacyServiceImpl,
                                  @Qualifier("lockerLegacyServiceImpl") final LegacyService lockerLegacyServiceImpl,
                                  @Qualifier("pickerLegacyServiceImpl") final LegacyService pickerLegacyServiceImpl) {
        legacyServices.put(SubSetType.REGIONS, regionLegacyServiceImpl);
        legacyServices.put(SubSetType.DOMAIN_ATTRIBUTE, domainAttributeLegacyServiceImpl);
        legacyServices.put(SubSetType.LOCKER_REGIONS, lockerLegacyServiceImpl);
        legacyServices.put(SubSetType.PICKER_REGION, pickerLegacyServiceImpl);
    }

    @Override
    public void batch(){
        legacyServices.get(SubSetType.DOMAIN_ATTRIBUTE).consolidate();
        legacyServices.get(SubSetType.REGIONS).consolidate();



        //mudou n vai mais solto
        //premissa: sem locker e picker solto
        //legacyServices.get(SubSetType.LOCKER_REGIONS).consolidate();
        //legacyServices.get(SubSetType.PICKER_REGION).consolidate();
    }

}
