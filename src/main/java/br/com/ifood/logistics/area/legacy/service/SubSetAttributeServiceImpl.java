package br.com.ifood.logistics.area.legacy.service;

import java.util.List;
import br.com.ifood.logistics.area.legacy.dao.SubSetAttributeDao;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("subSetAttributeService")
@Slf4j
public class SubSetAttributeServiceImpl implements SubSetAttributeService {

    @Autowired
    private SubSetAttributeDao subSetAttributeDao;

    @Override
    public SubSetAttributeDTO getBySubSetIdAndAttributeKeyName(Long subsetId, String attributeKeyName) {
        return subSetAttributeDao.getBySubSetIdAndAttributeKeyName(subsetId, attributeKeyName);
    }

    @Override
    public List<SubSetAttributeDTO> getBySubSetId(Long subsetId) {
        return subSetAttributeDao.getBySubSetId(subsetId);
    }

    @Override
    public List<SubSetAttributeDTO> getBySubSetAttributesBySubSetUUID(String uuid) {
        return subSetAttributeDao.getBySubSetAttributesBySubSetUUID(uuid);
    }
}
