package br.com.ifood.logistics.area.legacy.dao;

public interface SubRegionDao {
    void insert(String uuid, String regionUuid, String type);
}
