package br.com.ifood.logistics.area.legacy.service;

public interface SubRegionService {
    void insert(String uuid, String regionUuid, String type);
}
