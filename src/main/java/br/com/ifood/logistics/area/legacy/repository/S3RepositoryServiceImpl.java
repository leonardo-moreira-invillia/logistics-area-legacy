package br.com.ifood.logistics.area.legacy.repository;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import br.com.ifood.logistics.area.legacy.exception.LogisticsAreaException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service("s3RepositoryService")
@Slf4j
public class S3RepositoryServiceImpl implements S3RepositoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3RepositoryServiceImpl.class);

    private final AmazonS3 s3;
    private final String bucketName;

    @Autowired
    public S3RepositoryServiceImpl(final AmazonS3 s3, @Value("${area.s3.bucket.name}") final String bucketName) {
        this.s3 = s3;
        this.bucketName = bucketName;
    }

    @Override
    public String getFile(String path) {
        if(StringUtils.isEmpty(path))
            return null;

        if(!doesObjectExists(path))
            return null;

        try {
            final int delimiterIndex = path.indexOf('/');
            final String pathFile = path.substring(delimiterIndex + 1);
            String s3Object = s3.getObjectAsString(bucketName, pathFile);
            return s3Object;
        } catch (AmazonServiceException e) {
            LOGGER.error("Cannot find/read a S3 file, key: {}", path);
            throw new LogisticsAreaException(e.getMessage());
        }
    }

    @Override
    public final boolean doesObjectExists(final String path) {

        if(StringUtils.isEmpty(path))
            return false;

        try {
            final int delimiterIndex = path.indexOf('/');
            final String pathFile = path.substring(delimiterIndex + 1);
            return s3.doesObjectExist(bucketName, pathFile);
        } catch (AmazonServiceException e) {
            LOGGER.error("Cannot see if the object exists, key: {}", path);
            throw new LogisticsAreaException(e.getMessage());
        }
    }

}
