package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;

public interface SubSetSystemConfigurationDao {
    List<SubSetAttributeDTO> getSystemConfigurationBySubSetUUID(String uuid);
    SubSetAttributeDTO createFromMap(Map<String, Object> map);
}
