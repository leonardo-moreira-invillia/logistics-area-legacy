package br.com.ifood.logistics.area.legacy.service;

import br.com.ifood.logistics.area.legacy.dao.RegionDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("regionServiceImpl")
@Slf4j
public class RegionServiceImpl implements RegionService {

    @Autowired
    private RegionDao regionDao;

    @Override
    public void insert(String uuid, String tenant) {
        regionDao.insert(uuid, tenant);
    }
}
