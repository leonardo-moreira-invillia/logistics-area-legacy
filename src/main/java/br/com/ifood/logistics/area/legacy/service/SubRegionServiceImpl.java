package br.com.ifood.logistics.area.legacy.service;

import br.com.ifood.logistics.area.legacy.dao.SubRegionDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("subRegionServiceImpl")
@Slf4j
public class SubRegionServiceImpl implements SubRegionService {

    @Autowired
    private SubRegionDao subRegionDao;

    @Override
    public void insert(String uuid, String regionUuid, String type) {
        subRegionDao.insert(uuid, regionUuid, type);
    }
}
