package br.com.ifood.logistics.area.legacy.dao;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import br.com.ifood.logistics.area.legacy.model.Source;
import br.com.ifood.logistics.area.legacy.model.SubSetAttributeDTO;
import br.com.ifood.logistics.area.legacy.model.SubSetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SubSetSystemConfigurationDaoImpl implements SubSetSystemConfigurationDao {

    @Autowired
    Map<Source, JdbcTemplate> templateMap;
    private final String queryAllSubSeAttributesBySubSetUUID = "select b.uuid, b.partitions_name, 'FLEET_CONFIG' as domain,  a.key as attributes_key_name, a.value as value \n" +
                                                                "from system_configuration_by_subset a \n" +
                                                                "inner join subset b on b.id = a.subset_id \n" +
                                                                "where b.uuid::text = ?";

    @Override
    public SubSetAttributeDTO createFromMap(Map<String, Object> map) {
        SubSetAttributeDTO dto = new SubSetAttributeDTO();
        dto.setKey(map.get("attributes_key_name").toString());
        dto.setValue(map.get("value").toString());

        if(map.get("domain") != null)
            dto.setDomain(map.get("domain").toString());

        if(map.get("partitions_name") != null)
            dto.setAreaType(SubSetType.valueOf(map.get("partitions_name").toString()).getAreaType());

        return dto;
    }

    @Override
    public List<SubSetAttributeDTO> getSystemConfigurationBySubSetUUID(String uuid) {
        List<Map<String, Object>> rows = templateMap.get(Source.FLEET).queryForList(queryAllSubSeAttributesBySubSetUUID, new Object[] {uuid});
        List<SubSetAttributeDTO> dtoList = rows.stream().map(item -> this.createFromMap(item)).collect(Collectors.toList());
        return dtoList;
    }

}
