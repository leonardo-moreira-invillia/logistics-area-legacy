package br.com.ifood.logistics.area.legacy;

import br.com.ifood.logistics.area.legacy.service.LegacyBatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class LogisticsAreaLegacyApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogisticsAreaLegacyApplication.class, args);
    }

    @Autowired
    LegacyBatchService legacyServiceImpl;


    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        System.setProperty("com.amazonaws.services.s3.disableGetObjectMD5Validation","true");

        return args -> {
            legacyServiceImpl.batch();
        };
    }
}
